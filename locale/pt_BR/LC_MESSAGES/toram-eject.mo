��          �            h     i          �  #   �  !   �  3   �  "        B  #   `  !   �  !   �  *   �  "   �       �  )     �     �       Q   $  &   v  w   �  %     3   ;  :   o  8   �  9   �  /     ;   M  C   �                  	      
                                                        Automatic toram Eject Could not close %s Could not eject %s Do you want the LiveCD/DVD ejected? Do you want the drive closed now? Eject LiveCD/DVD via a simple GUI if %s is enabled. Feel free to remove the CD or DVD. The %s boot option succeeded. The boot media is no longer needed. boot device: %s is not removable. boot device: %s is still mounted! did not closing drive as per user request. did not eject as per user request. no BOOT_DEV given! Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-25 00:11+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/anticapitalista/antix-development/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
 Ejetar 'toram' automaticamente Não foi possível fechar %s Não foi possível ejetar %s Você quer ejetar o dispositivo de instalação externa executável (LiveCD/DVD)? Você quer fechar o dispositivo agora? Ejetar o dispositivo de instalação externa executável através de uma interface gráfica GUI, se %s estiver ativado. Você já pode remover o CD ou o DVD. A inicialização pela opção %s foi bem-sucedida. O dispositivo de inicialização não é mais necessário. o dispositivo de inicialização: %s não é removível. o dispositivo de inicialização: %s ainda está montado! o dispositivo não foi fechado como solicitado. não foi ejetado de acordo com a solicitação do usuário. Não foi indicado nenhum dispositivo de inicialização (BOOT_DEV)! 